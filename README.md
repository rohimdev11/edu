
# Technical Test - Backend Engineer

## Run Locally

Clone the project

```bash
  docker compose cant running yet
  git clone https://gitlab.com/rohimdev11/edu.git
```

Go to the project directory

```bash
  cd technical-test-edufund
```

Setup env

```bash
  PORT=2002
  ENV=dev
  DATABASE_URL=postgres://postgres:postgres@localhost:5432/ts_edufund_db
  TOKEN_SECRET=e3886ce442808631e8328d3adaa7518c330d3857017f57bf240122a090784892
```

Push and migrate db

```bash
  make migrate && make db
```

Install dependencies

```bash
  go get .
  go mod tidy
```

Start the server

```bash
  make start
```


## Authors

- [@Nur Rohim](https://github.com/nurrohim11)

