start:
	nodemon --exec go run server.go --signal SIGTERM

documentation:
	swag init -g server.go

migrate:
	go run github.com/prisma/prisma-client-go migrate dev --name init
	
db:
	go run github.com/prisma/prisma-client-go db push
	