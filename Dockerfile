FROM golang:1.18 as build

WORKDIR /app

# add go modules lockfiles
COPY go.mod go.sum ./
RUN go mod tidy

RUN go get github.com/prisma/prisma-client-go
# prefetch the binaries, so that they will be cached and not downloaded on each change
RUN go run github.com/prisma/prisma-client-go prefetch

COPY . ./

# generate the Prisma Client Go client
RUN go run github.com/prisma/prisma-client-go generate

# RUN go run github.com/prisma/prisma-client-go push
# or, if you use go generate to run the generator, use the following line instead
# RUN go generate ./...

# build the binary with all dependencies
RUN go build -o /main .

CMD ["/main"]
