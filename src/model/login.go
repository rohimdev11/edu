package model

type LoginInput struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type DataUser struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type LoginOutput struct {
	ID           string   `json:"id"`
	Data         DataUser `json:"data"`
	AccessToken  string   `json:"access_token"`
	ExpiresIn    int      `json:"expires_in"`
	RefreshToken string   `json:"refresh_token"`
}

type RegisterInput struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type RegisterOutput struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Username string `json:"username"`
}
