package model

type BaseListParams struct {
	Page   int64
	Limit  int64
	Search string
	Sort   string
}

type BaseResponse struct {
	Code     uint16      `json:"code" example:"200"`
	Message  string      `json:"message" example:"Success"`
	Response interface{} `json:"response" `
}

type BaseListResponse struct {
	Docs          interface{} `json:"docs" examle:"[]"`
	TotalDocs     int64       `json:"totalDocs" example:"3"`
	Limit         int64       `json:"limit" example:"10"`
	Page          int64       `json:"page" example:"1"`
	TotalPages    int64       `json:"totalPages" example:"1"`
	PagingCounter int64       `json:"pagingCounter" example:"1"`
	HasPrevPage   bool        `json:"hasPrevPage" example:"false"`
	HasNextPage   bool        `json:"hasNextPage" example:"false"`
	PrevPage      int64       `json:"prevPage" example:"1"`
	NextPage      int64       `json:"nextPage" example:"1"`
}

type ErrorResponse struct {
	BaseResponse
	Severity string `json:"severity" example:"high"`
	Message  string `json:"message" example:"error"`
}

type ListParams struct {
	BaseListParams BaseListParams
	Sort           string
	Date           string
}
