package model

import "github.com/golang-jwt/jwt"

type SignedDetails struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Email     string `json:"email"`
	TokenType string `json:"token_type"`
	jwt.StandardClaims
}

type RefreshTokenInput struct {
	RefreshToken string `json:"refresh_token"`
}

type Token struct {
	ID           string `json:"id"`
	AccessToken  string `json:"access_token"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
}
