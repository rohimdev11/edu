package model

type TopupInput struct {
	BankCode string `json:"bank_code"`
	Amount   int    `json:"amount"`
	Type     string `json:"type"`
}

type TopupOutput struct {
	BankCode string `json:"bank_code"`
	Balance  int    `json:"balance"`
}

type TransferInput struct {
	UserId string `json:"user_id"`
	Amount int    `json:"amount"`
	Type   string `json:"type"`
}

type TransferOutput struct {
	Amount  int `json:"amount"`
	Balance int `json:"balance"`
}
