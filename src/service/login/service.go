package login

import (
	"errors"
	"technical-test-edufund/src/model"
)

type Service interface {
	FindUserById(userId string) (response UserResponse, err error)
	ValidateLogin(params *model.LoginInput) (err error)
	Login(params *model.LoginInput) (response LoginResponse, err error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) ValidateLogin(params *model.LoginInput) (err error) {
	if params.Username == "" {
		return errors.New("Username cannot be empty")
	}
	if params.Password == "" {
		return errors.New("Password cannot be empty")
	}
	return nil
}

func (s *service) Login(params *model.LoginInput) (response LoginResponse, err error) {

	entity := LoginEntity{
		Username: params.Username,
		Password: params.Password,
	}

	response, err = s.repository.Login(&entity)
	if err != nil {
		return
	}
	return
}

func (s *service) FindUserById(userId string) (response UserResponse, err error) {
	response, err = s.repository.FindUserById(userId)
	if err != nil {
		return
	}
	return
}
