package login

import (
	"errors"
	"fmt"
	"technical-test-edufund/prisma/db"
	"technical-test-edufund/src/util"

	"golang.org/x/crypto/bcrypt"
)

type Repository interface {
	FindUserById(userId string) (entity UserResponse, err error)
	Login(params *LoginEntity) (result LoginResponse, err error)
}

type repository struct {
	db *db.PrismaClient
}

func NewRepository(db *db.PrismaClient) *repository {
	return &repository{db}
}

func (r *repository) Login(params *LoginEntity) (result LoginResponse, err error) {
	ctx, cancel := util.InitContext()
	defer cancel()
	result = LoginResponse{}

	user, err := r.db.User.FindFirst(
		db.User.Username.Equals(params.Username),
	).Exec(ctx)

	if err != nil && err != db.ErrNotFound {
		fmt.Println(err)
		return result, errors.New("Server error")
	}

	if err == db.ErrNotFound {
		return result, errors.New("User is not found")
	}

	pass, _ := user.Password()
	err = bcrypt.CompareHashAndPassword([]byte(pass), []byte(params.Password))

	if err != nil {
		return
	}

	result.ID = user.ID
	result.Name, _ = user.Name()
	result.Email, _ = user.Email()
	return
}

func (r *repository) FindUserById(userId string) (entity UserResponse, err error) {
	ctx, cancel := util.InitContext()
	defer cancel()
	entity = UserResponse{}

	user, err := r.db.User.FindUnique(
		db.User.ID.Equals(userId),
	).Exec(ctx)

	if err != nil && err != db.ErrNotFound {
		return entity, errors.New("Server error")
	}

	if err == db.ErrNotFound {
		return entity, errors.New("User is not found")
	}

	entity.ID = user.ID
	entity.Name, _ = user.Name()
	entity.Email, _ = user.Email()

	return
}

func (r *repository) RegisterUser(params *RegisterEntity) (result RegisterResponse, err error) {
	ctx, cancel := util.InitContext()
	defer cancel()

	result = RegisterResponse{}

	create, err := r.db.User.CreateOne(
		db.User.Name.Set(params.Name),
		db.User.Username.Set(params.Username),
		db.User.Password.Set(params.Password),
		db.User.Email.Set(params.Email),
	).Exec(ctx)

	if err != nil {
		return
	}

	result.ID = create.ID
	result.Name, _ = create.Name()
	result.Username, _ = create.Username()
	result.Email, _ = create.Email()

	return
}
