package login

type UserResponse struct {
	ID    string
	Name  string
	Email string
}

type LoginEntity struct {
	Username string
	Password string
}

type LoginResponse struct {
	ID    string
	Name  string
	Email string
}

type RegisterEntity struct {
	Name     string
	Username string
	Email    string
	Password string
}

type RegisterResponse struct {
	ID       string
	Name     string
	Username string
	Email    string
}
