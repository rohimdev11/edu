package register

import (
	"errors"
	"technical-test-edufund/prisma/db"
	"technical-test-edufund/src/util"
)

type Repository interface {
	FindUserById(userId string) (entity UserResponse, err error)
	CheckEmail(email string) (val bool, err error)
	CheckUsername(username string) (val bool, err error)
	RegisterUser(params *RegisterEntity) (result RegisterResponse, err error)
}

type repository struct {
	db *db.PrismaClient
}

func NewRepository(db *db.PrismaClient) *repository {
	return &repository{db}
}

func (r *repository) FindUserById(userId string) (entity UserResponse, err error) {
	ctx, cancel := util.InitContext()
	defer cancel()
	entity = UserResponse{}

	user, err := r.db.User.FindUnique(
		db.User.ID.Equals(userId),
	).Exec(ctx)

	if err != nil && err != db.ErrNotFound {
		return entity, errors.New("Server error")
	}

	if err == db.ErrNotFound {
		return entity, errors.New("User is not found")
	}

	entity.ID = user.ID
	entity.Name, _ = user.Name()
	entity.Email, _ = user.Email()

	return
}

func (r *repository) CheckEmail(email string) (val bool, err error) {
	ctx, cancel := util.InitContext()
	defer cancel()

	user, err := r.db.User.FindMany(
		db.User.Email.Equals(email),
	).Exec(ctx)

	if err != nil {
		return false, errors.New("Server error")
	}

	if len(user) > 0 {
		return false, errors.New("Email is not available")
	}

	return true, nil
}

func (r *repository) CheckUsername(username string) (val bool, err error) {
	ctx, cancel := util.InitContext()
	defer cancel()

	user, err := r.db.User.FindMany(
		db.User.Username.Equals(username),
	).Exec(ctx)

	if err != nil {
		return false, errors.New("Server error")
	}

	if len(user) > 0 {
		return false, errors.New("Username is not available")
	}

	return true, nil
}

func (r *repository) RegisterUser(params *RegisterEntity) (result RegisterResponse, err error) {
	ctx, cancel := util.InitContext()
	defer cancel()

	result = RegisterResponse{}

	create, err := r.db.User.CreateOne(
		db.User.Name.Set(params.Name),
		db.User.Username.Set(params.Username),
		db.User.Password.Set(params.Password),
		db.User.Email.Set(params.Email),
	).Exec(ctx)

	if err != nil {
		return
	}

	result.ID = create.ID
	result.Name, _ = create.Name()
	result.Username, _ = create.Username()
	result.Email, _ = create.Email()

	return
}
