package register

type UserResponse struct {
	ID    string
	Name  string
	Email string
}

type RegisterEntity struct {
	Name     string
	Username string
	Email    string
	Password string
}

type RegisterResponse struct {
	ID       string
	Name     string
	Username string
	Email    string
}
