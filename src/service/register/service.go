package register

import (
	"errors"
	"technical-test-edufund/src/helper"
	"technical-test-edufund/src/model"

	"golang.org/x/crypto/bcrypt"
)

type Service interface {
	FindUserById(userId string) (response UserResponse, err error)
	ValidateRegister(params *model.RegisterInput) (err error)
	Register(params *model.RegisterInput) (response RegisterResponse, err error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) FindUserById(userId string) (response UserResponse, err error) {
	response, err = s.repository.FindUserById(userId)
	if err != nil {
		return
	}
	return
}

func (s *service) ValidateRegister(params *model.RegisterInput) (err error) {
	if params.Name == "" {
		return errors.New("Name cannot be empty")
	}
	if params.Email == "" {
		return errors.New("Email cannot be empty")
	}
	if !helper.IsEmailValid(params.Email) {
		return errors.New("Email is wrong format")
	}
	checkEmail, err := s.repository.CheckEmail(params.Email)
	if !checkEmail {
		return err
	}
	if params.Username == "" {
		return errors.New("Username cannot be empty")
	}
	isUsernameValid, err := helper.IsUsernameValid(params.Username)
	if !isUsernameValid {
		return err
	}
	checkUsername, err := s.repository.CheckUsername(params.Username)
	if !checkUsername {
		return err
	}
	if params.Password == "" {
		return errors.New("Password cannot be empty")
	}
	isPasswordValid, err := helper.IsPasswordValid(params.Password)
	if !isPasswordValid {
		return err
	}
	return nil
}

func (s *service) Register(params *model.RegisterInput) (response RegisterResponse, err error) {
	response = RegisterResponse{}
	body := RegisterEntity{}

	password := []byte(params.Password)
	// Hashing the password with the default cost of 10
	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		return
	}

	body.Name = params.Name
	body.Email = params.Email
	body.Username = params.Username
	body.Password = string(hashedPassword)

	response, err = s.repository.RegisterUser(&body)
	if err != nil {
		return
	}
	return
}
