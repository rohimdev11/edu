package helper

import (
	"fmt"
	"math"
	"net"
	"technical-test-edufund/src/model"

	"github.com/go-playground/validator/v10"
)

func GenerateResponse(code uint16, message string, response interface{}) model.BaseResponse {
	var baseResponse model.BaseResponse
	baseResponse.Code = code
	baseResponse.Message = message
	baseResponse.Response = response

	return baseResponse
}

func FormatValidationError(err error) []string {
	var errors []string

	for _, e := range err.(validator.ValidationErrors) {
		errors = append(errors, e.Error())
	}

	return errors
}

func Round(val float64, precision uint) float64 {
	ratio := math.Pow(10, float64(precision))
	return math.Round(val*ratio) / ratio
}

func FindValue(slice []string, val string) bool {
	for _, item := range slice {
		if item == val {
			return true
		}
	}
	return false
}

func ErrNotFound() string {
	return "Data not found"
}

func GetIpAddress() string {
	conn, error := net.Dial("udp", "8.8.8.8:80")
	if error != nil {
		return ""
	}

	defer conn.Close()
	ipAddress := conn.LocalAddr().(*net.UDPAddr)
	return fmt.Sprintf("%d", ipAddress.IP)
}
