package helper

import (
	"errors"
	"regexp"
	"unicode"
)

func IsEmailValid(e string) bool {
	emailRegex := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	return emailRegex.MatchString(e)
}

func IsUsernameValid(s string) (bool, error) {
	var (
		hasMinLen = false
		hasSpace  = false
	)
	if len(s) >= 5 {
		hasMinLen = true
	}
	for _, char := range s {
		switch {
		case unicode.IsSpace(char):
			hasSpace = true
		}
	}
	if !hasMinLen {
		return false, errors.New("Username must be longer than 5 character")
	}
	if hasSpace {
		return false, errors.New("Username cannot there is white space")
	}
	return hasMinLen && !hasSpace, nil
}

func IsPasswordValid(s string) (bool, error) {
	var (
		hasMinLen = false
		// hasUpper  = false
		// hasLower  = false
		hasNumber = false
		hasSpace  = false
		// hasSpecial = false
	)
	if len(s) >= 5 {
		hasMinLen = true
	}
	for _, char := range s {
		switch {
		// case unicode.IsUpper(char):
		// 	hasUpper = true
		// case unicode.IsLower(char):
		// 	hasLower = true
		case unicode.IsNumber(char):
			hasNumber = true
		case unicode.IsSpace(char):
			hasSpace = true
			// case unicode.IsPunct(char) || unicode.IsSymbol(char):
			// 	hasSpecial = true
		}
	}
	// if !hasUpper {
	// 	return false, errors.New("Password harus terdapat minimal 1 huruf besar")
	// }
	// if !hasLower {
	// 	return false, errors.New("Password harus ada huruf kecil")
	// }
	if !hasMinLen {
		return false, errors.New("Password must be longer than 5 character")
	}
	if hasSpace {
		return false, errors.New("Password cannot there is white space")
	}

	if !hasNumber {
		return false, errors.New("Password must contain at least 1 digit")
	}
	// return hasMinLen && hasUpper && hasLower && hasNumber, nil
	return hasMinLen && !hasSpace && hasNumber, nil
}

/*
 * Password rules:
 * at least 7 letters
 * at least 1 number
 * at least 1 upper case
 * at least 1 special character
 */
