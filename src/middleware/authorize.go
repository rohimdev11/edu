package middleware

import (
	"log"
	"net/http"
	"strings"
	"technical-test-edufund/src/config"
	"technical-test-edufund/src/helper"
	"technical-test-edufund/src/model"
	"technical-test-edufund/src/service/login"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

var LocalUser = make(map[string]interface{})
var TokekBackoffice = ""

func Authorize() gin.HandlerFunc {
	return func(c *gin.Context) {
		clientToken := c.Request.Header.Get("Authorization")

		if clientToken == "" {
			clientToken = c.Request.Header.Get("token")
		} else if strings.HasPrefix(clientToken, "Bearer ") {
			reqToken := c.Request.Header.Get("Authorization")
			splitToken := strings.Split(reqToken, "Bearer ")
			clientToken = splitToken[1]
		} else {
			c.JSON(http.StatusUnauthorized, helper.GenerateResponse(http.StatusUnauthorized, "Invalid authorization bearer", nil))
			c.Abort()
			return
		}

		if clientToken == "" {
			c.JSON(http.StatusUnauthorized, helper.GenerateResponse(http.StatusUnauthorized, "Invalid authorization bearer", nil))
			c.Abort()
			return
		}
		// handle access token
		claims, err := ValidateToken(clientToken)

		if err != "" {
			c.JSON(http.StatusUnauthorized, helper.GenerateResponse(http.StatusUnauthorized, "Token expired", nil))
			c.Abort()
			return
		}

		if claims.TokenType == "refresh_token" {
			c.JSON(http.StatusUnauthorized, helper.GenerateResponse(http.StatusUnauthorized, "Token is not valid", nil))
			c.Abort()
			return
		}

		c.Set("userId", claims.ID)
		c.Set("name", claims.Name)
		c.Set("email", claims.Email)
		c.Next()
	}
}

func ValidateToken(signedToken string) (claims *model.SignedDetails, msg string) {
	local, _ := time.LoadLocation("Asia/Jakarta")
	token, err := jwt.ParseWithClaims(
		signedToken,
		&model.SignedDetails{},
		func(t *jwt.Token) (interface{}, error) {
			return []byte(config.GetConfig().Token.TokenSecret), nil
		},
	)

	if err != nil {
		msg = err.Error()
		return
	}

	claims, ok := token.Claims.(*model.SignedDetails)
	if !ok {
		msg = err.Error()
		return
	}

	if claims.ExpiresAt < time.Now().In(local).Unix() {
		msg = err.Error()
		return
	}

	return claims, msg
}

func GenerateToken(userId string, loginService login.Service) (signedAccessToken string, signedRefreshToken string, expiresIn int, err error) {
	local, _ := time.LoadLocation("Asia/Jakarta")
	expired := time.Now().In(local).Add(time.Minute * time.Duration(30))

	user, err := loginService.FindUserById(userId)
	if err != nil {
		log.Panic(err)
		return
	}

	accessClaims := &model.SignedDetails{
		ID:        userId,
		Name:      user.Name,
		Email:     user.Email,
		TokenType: "access_token",
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expired.Unix(),
		},
	}

	refreshClaims := &model.SignedDetails{
		ID:        userId,
		Name:      user.Name,
		Email:     user.Email,
		TokenType: "refresh_token",
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().In(local).Add(time.Hour * time.Duration(24*7)).Unix(),
		},
	}

	accessToken, err := jwt.NewWithClaims(jwt.SigningMethodHS256, accessClaims).SignedString([]byte(config.GetConfig().Token.TokenSecret))
	if err != nil {
		log.Panic(err)
		return
	}

	refreshToken, err := jwt.NewWithClaims(jwt.SigningMethodHS256, refreshClaims).SignedString([]byte(config.GetConfig().Token.TokenSecret))
	if err != nil {
		log.Panic(err)
		return
	}

	return accessToken, refreshToken, 1800, err
}
