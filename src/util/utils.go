package util

import (
	"context"
	"technical-test-edufund/src/model"
	"time"
)

func InitContext() (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	return ctx, cancel
}

func GenerateResponse(severity string, code uint16, message string) model.ErrorResponse {
	var baseResponse model.BaseResponse
	baseResponse.Code = code

	var response model.ErrorResponse
	response.BaseResponse = baseResponse
	response.Severity = severity
	response.Message = message

	return response
}
