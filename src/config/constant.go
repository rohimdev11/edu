package config

import (
	"log"
	"os"
)

type config struct {
	Environment *string
	Server      *serverConfig
	Token       *token
}

type serverConfig struct {
	Hostname string `default:"localhost"`
	Port     string `default:"2002"`
	Cors     string `default:"*"`
	BaseURL  string
}

type token struct {
	TokenSecret string
}

var configInstance *config

func GetConfig() *config {
	if configInstance == nil {
		server := getServerConfig()
		token := getToken()

		env := os.Getenv("ENV")
		if env == "" {
			env = "dev"
		}

		configInstance = &config{
			Environment: &env,
			Server:      &server,
			Token:       &token,
		}
		log.Println("Get Config Instance.")
	}

	return configInstance
}

func getServerConfig() serverConfig {
	port := os.Getenv("PORT")
	if port == "" {
		port = "2002"
	}

	baseURL := os.Getenv("BASE_URL")
	if baseURL == "" {
		baseURL = "localhost:2002"
	}

	server := serverConfig{
		Port:    port,
		BaseURL: baseURL,
	}

	return server
}

func getToken() token {
	tokenSecret := os.Getenv("TOKEN_SECRET")
	if tokenSecret == "" {
		tokenSecret = ""
	}

	token := token{
		TokenSecret: tokenSecret,
	}

	return token
}
