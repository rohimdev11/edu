package controller

import (
	"fmt"
	"net/http"
	"technical-test-edufund/src/helper"
	"technical-test-edufund/src/middleware"
	middlewares "technical-test-edufund/src/middleware"
	"technical-test-edufund/src/model"
	"technical-test-edufund/src/service/login"

	"github.com/gin-gonic/gin"
)

type loginController struct {
	loginService login.Service
}

func NewLoginController(loginService login.Service) *loginController {
	return &loginController{loginService}
}

func (l *loginController) LoginUser(ctx *gin.Context) {
	var loginInput model.LoginInput

	if err := ctx.BindJSON(&loginInput); err != nil {
		ctx.JSON(http.StatusOK, helper.GenerateResponse(http.StatusBadRequest, "Bad request", nil))
		return
	}

	if err := l.loginService.ValidateLogin(&loginInput); err != nil {
		ctx.JSON(http.StatusBadRequest, helper.GenerateResponse(http.StatusBadRequest, err.Error(), nil))
		return
	}

	resp, err := l.loginService.Login(&loginInput)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, helper.GenerateResponse(http.StatusBadRequest, err.Error(), nil))
		return
	}

	accessToken, refreshToken, expiresIn, err := middlewares.GenerateToken(resp.ID, l.loginService)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, helper.GenerateResponse(http.StatusInternalServerError, "Server error", nil))
		return
	}

	result := model.LoginOutput{
		ID: resp.ID,
		Data: model.DataUser{
			Name:  resp.Name,
			Email: resp.Email,
		},
		AccessToken:  fmt.Sprintf("Bearer %s", accessToken),
		RefreshToken: refreshToken,
		ExpiresIn:    expiresIn,
	}

	ctx.JSON(http.StatusOK, helper.GenerateResponse(http.StatusOK, "Successfully login", result))

}

func (l *loginController) RefreshToken(c *gin.Context) {
	var refresTokenInput model.RefreshTokenInput

	if err := c.BindJSON(&refresTokenInput); err != nil {
		c.JSON(http.StatusOK, helper.GenerateResponse(http.StatusBadRequest, "Server error", nil))
		return
	}

	if refresTokenInput.RefreshToken == "" {
		c.JSON(http.StatusOK, helper.GenerateResponse(http.StatusBadRequest, "Refresh token cannot be found", nil))
		return
	}

	claims, _ := middlewares.ValidateToken(refresTokenInput.RefreshToken)
	if claims.TokenType != "refresh_token" {
		c.JSON(http.StatusOK, helper.GenerateResponse(http.StatusBadRequest, "Invalid refresh token", nil))
		return
	}

	accessToken, refreshToken, expiresIn, err := middleware.GenerateToken(claims.ID, l.loginService)
	if err != nil {
		c.JSON(http.StatusOK, helper.GenerateResponse(http.StatusInternalServerError, err.Error(), nil))
		return
	}

	response := new(model.Token)
	response.AccessToken = accessToken
	response.RefreshToken = refreshToken
	response.ID = claims.ID
	response.ExpiresIn = expiresIn
	c.JSON(http.StatusOK, helper.GenerateResponse(http.StatusOK, "Successfull", response))
}
