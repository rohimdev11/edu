package controller

import (
	"net/http"
	"technical-test-edufund/src/helper"
	"technical-test-edufund/src/model"
	"technical-test-edufund/src/service/register"

	"github.com/gin-gonic/gin"
)

type registerController struct {
	registerService register.Service
}

func NewRegisterController(registerService register.Service) *registerController {
	return &registerController{registerService}
}

func (l *registerController) RegisterUser(ctx *gin.Context) {
	var registerInput model.RegisterInput

	if err := ctx.BindJSON(&registerInput); err != nil {
		ctx.JSON(http.StatusOK, helper.GenerateResponse(http.StatusBadRequest, "Bad request", nil))
		return
	}

	if err := l.registerService.ValidateRegister(&registerInput); err != nil {
		ctx.JSON(http.StatusBadRequest, helper.GenerateResponse(http.StatusBadRequest, err.Error(), nil))
		return
	}

	resp, err := l.registerService.Register(&registerInput)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, helper.GenerateResponse(http.StatusInternalServerError, "Server error", nil))
		return
	}

	result := model.RegisterOutput{
		ID:       resp.ID,
		Name:     resp.Name,
		Email:    resp.Email,
		Username: resp.Username,
	}

	ctx.JSON(http.StatusOK, helper.GenerateResponse(http.StatusOK, "Successfully register", result))

}
