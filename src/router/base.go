package router

import (
	"log"
	"net/http"
	prisma "technical-test-edufund/prisma/db"
	configs "technical-test-edufund/src/config"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func Handler() {
	gin.SetMode(gin.ReleaseMode)

	router := gin.New()
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "GET", "POST", "OPTIONS", "DELETE"},
		AllowHeaders:     []string{"*"},
		AllowCredentials: true,
	}))

	publicRoute := router.Group("/api/user-service/v0")

	client := prisma.NewClient()
	if err := client.Prisma.Connect(); err != nil {
		log.Println(err)
		panic(err)
	}
	InitLoginRoute(publicRoute, client)
	InitRegisterRoute(publicRoute, client)
	// health check
	router.GET("/api/user-service/health-check", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Heath check success",
		})
	})

	serverPort := configs.GetConfig().Server.Port
	log.Println("Server at running port :", serverPort)
	router.Run(":" + configs.GetConfig().Server.Port)
}
