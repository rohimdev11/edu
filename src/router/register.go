package router

import (
	"technical-test-edufund/prisma/db"
	"technical-test-edufund/src/controller"
	"technical-test-edufund/src/service/register"

	"github.com/gin-gonic/gin"
)

func InitRegisterRoute(app *gin.RouterGroup, db *db.PrismaClient) {
	registerRepository := register.NewRepository(db)
	registerUser := register.NewService(registerRepository)
	registerController := controller.NewRegisterController(registerUser)
	registerRoute := app.Group("register")
	registerRoute.POST("/", registerController.RegisterUser)
}
