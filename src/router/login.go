package router

import (
	"technical-test-edufund/prisma/db"
	"technical-test-edufund/src/controller"
	"technical-test-edufund/src/service/login"

	"github.com/gin-gonic/gin"
)

func InitLoginRoute(app *gin.RouterGroup, db *db.PrismaClient) {
	loginRepository := login.NewRepository(db)
	loginService := login.NewService(loginRepository)
	loginController := controller.NewLoginController(loginService)
	loginRoute := app.Group("login")
	loginRoute.POST("/", loginController.LoginUser)
	loginRoute.POST("/refresh-token", loginController.RefreshToken)
}
