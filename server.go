package main

import (
	"technical-test-edufund/src/config"
	"technical-test-edufund/src/router"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	config.GetConfig()
	router.Handler()
}
