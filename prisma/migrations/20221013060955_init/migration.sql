/*
  Warnings:

  - You are about to drop the `user_balance` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `user_balance_history` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "user_balance" DROP CONSTRAINT "user_balance_user_id_fkey";

-- AlterTable
ALTER TABLE "user" ADD COLUMN     "name" TEXT,
ADD COLUMN     "password" TEXT,
ADD COLUMN     "test" TEXT,
ALTER COLUMN "id" SET DEFAULT (gen_random_uuid());

-- DropTable
DROP TABLE "user_balance";

-- DropTable
DROP TABLE "user_balance_history";

-- DropEnum
DROP TYPE "balance_type";
