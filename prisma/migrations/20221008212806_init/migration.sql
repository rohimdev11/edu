-- CreateEnum
CREATE TYPE "balance_type" AS ENUM ('debit', 'kredit');

-- CreateTable
CREATE TABLE "user" (
    "id" UUID NOT NULL DEFAULT (gen_random_uuid()),
    "username" TEXT,
    "email" TEXT,
    "created_at" TIMESTAMPTZ(6) DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(6),

    CONSTRAINT "user_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user_balance" (
    "id" UUID NOT NULL DEFAULT (gen_random_uuid()),
    "user_id" UUID NOT NULL,
    "balance" TEXT,
    "balance_achieve" INTEGER NOT NULL DEFAULT 0,

    CONSTRAINT "user_balance_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "user_balance_history" (
    "id" UUID NOT NULL DEFAULT (gen_random_uuid()),
    "user_balance_id" UUID NOT NULL,
    "balance_before" INTEGER NOT NULL DEFAULT 0,
    "balance_after" INTEGER NOT NULL DEFAULT 0,
    "activity" TEXT,
    "type" "balance_type" NOT NULL DEFAULT E'debit',
    "ip" TEXT,
    "location" TEXT,
    "user_agent" TEXT,
    "author" TEXT,

    CONSTRAINT "user_balance_history_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "user_d_idx2" ON "user_balance"("user_id");

-- AddForeignKey
ALTER TABLE "user_balance" ADD CONSTRAINT "user_balance_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "user"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
